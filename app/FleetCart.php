<?php

namespace FleetCart;

class FleetCart
{
    /**
     * The FleetCart version.
     *
     * @var string
     */
    const VERSION = '2.0';
}
